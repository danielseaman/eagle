<!DOCTYPE html>
<html lang="en">
<head>
	<title>Eagle Financial Services</title>
	<meta charset="utf-8">
	<meta name="author" content="pixelhint.com">
	<meta name="description" content="Sublime Stunning free HTML5/CSS3 website template"/>
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/fancybox-thumbs.css">
	<link rel="stylesheet" type="text/css" href="css/fancybox-buttons.css">
	<link rel="stylesheet" type="text/css" href="css/fancybox.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/fancybox.js"></script>
    <script type="text/javascript" src="js/fancybox-buttons.js"></script>
    <script type="text/javascript" src="js/fancybox-media.js"></script>
    <script type="text/javascript" src="js/fancybox-thumbs.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>
<body>

	<section class="billboard light">
		<header class="wrapper light">

			<nav>
				<ul>
					<li><a href="/customers">Customers</a></li>
					<li><a href="/investments">Investments</a></li>
					<li><a href="/stocks">Stocks</a></li>
				</ul>
			</nav>
		</header>

		<div class="caption light animated wow fadeInDown clearfix">
			<h1>Eagle Financial Services</h1>
			<p>Meeting your financial needs since 1785</p>
			<hr>
		</div>
		<div class="shadow"></div>
	</section><!--  End billboard  -->


<!--  End footer  -->
    <script src='../ga.js'></script>
</body>
</html>